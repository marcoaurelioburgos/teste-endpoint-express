const express = require("express");
const app = express();

const concatenar = (vet) => {
    return vet.join(" ");
}

app.get("/concatenar/:vetor?", function(req,res){
    if (req.query.vetor){
        const palavras = req.query.vetor.split(",");
        const vetor = palavras.join(" ");
        res.send(`Seu vetor concatenado é: ${vetor}`);
    }
    else {
        res.send('Por favor, utilize o parâmetro "vetor" para passar os valores separados por ",".');
    }
})

app.listen(6789, function(req,res){
    console.log("Servidor rodando.");
})